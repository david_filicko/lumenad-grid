import React, {Component} from 'react'
import {DataGrid} from '@material-ui/data-grid'

class ShowGrid extends Component {
    constructor (props) {
        super(props)
        this.state = {
            grid: this.props.location.state.grid
        }
        this.handleAddColumn = this.handleAddColumn.bind(this);
    }

    findNewFieldName(grid){
        let attempts = 0;
        let fieldFound = false;
        do
        {
            ++attempts;
            fieldFound = false;
            grid.columns.forEach((column) => {
                if(column.field === "newColumn"+attempts){
                    fieldFound = true;
                }
            })
        }while (fieldFound)

        return "newColumn"+attempts;
    }

    handleAddColumn() {
        const grid = this.state.grid;
        let fieldName = this.findNewFieldName(grid)

        grid.columns.push({
            type: "string",
            field: fieldName,
            headerName: "new column [mixed]",
            width: 130
        });

        grid.rows.forEach((row) => {
            let newColValue = row[grid.columns[0].field] + row[grid.columns[2].field];

            if(newColValue.trim() === "" || newColValue === 0){
                newColValue = "[EMPTY]";
            }

            row[fieldName] = newColValue;
        })

        this.setState({
            grid: grid
        })
    }

    render() {
        const grid = JSON.parse(JSON.stringify(this.state.grid));

        grid.columns.forEach((column) => {
            column.width = 130;
            column.headerName = column.field + " [" + column.type+"]";
        })

        //retype bools
        grid.columns.forEach((column) => {
            if(column.type === "boolean"){
                grid.rows.forEach((row) => {
                    row[column.field] = (row[column.field] === "true")
                })
            }
        })

        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div style={{ height: 400, width: '100%' }}>
                        <DataGrid rows={grid.rows} columns={grid.columns} pageSize={10} />
                        <button onClick={this.handleAddColumn} >Add Column</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default ShowGrid
