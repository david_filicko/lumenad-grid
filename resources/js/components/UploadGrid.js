import React, {Component} from 'react'
import axios from 'axios';

class UploadGrid extends Component {
    constructor (props) {
        super(props)
        this.state = {
            selectedFile: undefined,
            error: undefined,
        }
        this.handleUpload = this.handleUpload.bind(this);
        this.fileInput = React.createRef();
    }

    handleUpload(event){
        event.preventDefault();
        const { history } = this.props

        history.push()
        const file = this.fileInput.current.files[0];
        const fileData = new FormData();
        fileData.append('gridFile', file, file.name)

        axios.post('/api/upload/', fileData)
            .then((response) => {
                this.props.history.push('/show', {
                    grid: response.data
                })
            })
            .catch(error => {
                this.setState({
                    error: error.response.data.error
                })
            });
    }

    renderError(){
        if(!!this.state.error){
            return (
                <span className='invalid-feedback'>
                    <strong>{this.state.error}</strong>
                </span>
            )
        }
    }

    render() {
        return (
            <div className='container py-4'>
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <div className='card'>
                            <div className='card-header'>Upload grid .csv</div>
                            <div className='card-body'>
                                <form onSubmit={this.handleUpload}>
                                    <div className='form-group'>
                                        <input
                                            type='file'
                                            ref={this.fileInput}
                                            className={`form-control ${!!this.state.error ? 'is-invalid' : ''}`}
                                        />
                                        {this.renderError()}
                                    </div>
                                    <button className='btn btn-primary' type="submit">Upload</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default UploadGrid
