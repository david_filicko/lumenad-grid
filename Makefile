start:
	docker-compose up -d

build:
	docker-compose up -d --build
	cp .env.example .env

deps:
	docker-compose exec php composer install
	docker-compose exec php npm install
	docker-compose exec php php artisan key:generate
	docker-compose exec php php artisan db:create
	docker-compose exec php php artisan migrate
	docker-compose exec php npm run prod
	docker-compose exec php chmod o+w storage/ -R

install: build deps

stop:
	docker-compose down

clean: stop
	rm -rf vendor/ public/hot public/storage .env
