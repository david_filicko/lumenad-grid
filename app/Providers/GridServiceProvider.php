<?php

namespace App\Providers;

use App\Contracts\GridParser;
use App\Contracts\GridValidator;
use App\Services\CsvGridService;
use Illuminate\Support\ServiceProvider;

class GridServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GridParser::class, CsvGridService::class);
        $this->app->bind(GridValidator::class, CsvGridService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
