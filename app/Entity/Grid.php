<?php


namespace App\Entity;


final class Grid implements \JsonSerializable
{
    /** @var Column[]  */
    private array $columns;

    /** @var Row[]  */
    private array $rows;

    /**
     * @param Column[] $columns
     * @param Row[] $rows
     */
    public function __construct(array $columns, array $rows)
    {
        $this->columns = $columns;
        $this->rows = $rows;
    }

    /**
     * @param Column[] $columns
     * @param Row[] $rows
     * @return Grid
     */
    public static function create(array $columns, array $rows): self
    {
        return new self($columns, $rows);
    }

    /**
     * @return Column[]
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @return Row[]
     */
    public function getRows(): array
    {
        return $this->rows;
    }

    public function jsonSerialize()
    {
        $json = new \stdClass();
        $json->columns = $this->columns;
        $json->rows = $this->rows;

        return $json;
    }
}
