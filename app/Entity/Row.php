<?php


namespace App\Entity;


final class Row implements \JsonSerializable
{
    private int $id;

    /** @var array<string,string>  */
    private array $fields = [];

    private function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function createEmptyRow(int $index): self
    {
        return new self($index);
    }

    public function addField(string $field, mixed $value): void
    {
        $this->fields[$field] = $value;
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    public function jsonSerialize()
    {
        $json = new \stdClass();
        $json->id = $this->id;

        foreach ($this->fields as $field => $value){
            $json->$field = $value;
        }

        return $json;
    }
}
