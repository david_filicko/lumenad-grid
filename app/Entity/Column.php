<?php


namespace App\Entity;


final class Column implements \JsonSerializable
{
    private string $field;
    private string $type;

    private function __construct(string $field, string $type)
    {
        $this->field = $field;
        $this->type = $type;
    }

    public static function create(string $field, string $type): self
    {
        return new self($field, $type);
    }

    public function jsonSerialize()
    {
        $json = new \stdClass();
        $json->field = $this->field;
        $json->type = $this->type;
        return $json;
    }
}
