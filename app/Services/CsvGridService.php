<?php


namespace App\Services;


use App\Contracts\GridParser;
use App\Contracts\GridValidator;
use App\Entity\Column;
use App\Entity\Grid;
use App\Entity\Row;

class CsvGridService implements GridParser, GridValidator
{
    private const ISO_FORMAT = "c";
    private const DATE_FORMAT = "Y-m-d";

    public function parse(string $gridString): Grid
    {
        $parsedCsvRows = $this->parseCsvString($gridString);

        $rows = $this->createRows($parsedCsvRows);

        $columns = $this->createColumns($parsedCsvRows);

        return Grid::create($columns, $rows);
    }

    public function validate(Grid $grid): void
    {
        if(\count($grid->getColumns()) === 0){
            throw new \Exception("Csv must contain at least 1 row");
        }

        $csvRows []= $grid->getColumns();
        foreach ($grid->getRows() as $row){
            $csvRows []= $row->getFields();
        }

        foreach ($csvRows as $index => $row) {
            $columnCount = \count($row);
            if($columnCount < 3){
                throw new \Exception("Contains less than 3 ($columnCount) columns on line ". ($index+1));
            }

            if($columnCount > 10){
                throw new \Exception("Contains more than 10 ($columnCount) columns on line ". ($index+1));
            }
        }
    }

    private function parseCsvString(string $gridString): array
    {
        //FIXME: \r not considered, also \n within quotes
        $nonEmptyLines = array_filter(
            explode("\n", $gridString),
            static function (string $row): bool {
                return $row !== "";
            }
        );

        return array_map(
            static function (string $line): array {
                return str_getcsv($line);
            },
            $nonEmptyLines
        );
    }


    /**
     * @param array $parsedCsvRows
     * @return Row[]
     */
    private function createRows(array $parsedCsvRows): array
    {
        $headers = $parsedCsvRows[0];
        $data = array_map(
            static function (array $row) use ($headers): array {
                return array_combine($headers, $row);
            },
            array_slice($parsedCsvRows, 1)
        );

        $rows = [];
        foreach ($data as $index => $dataRow) {
            $row = Row::createEmptyRow($index);
            $rows [] = $row;

            foreach ($dataRow as $field => $value) {
                $row->addField($field, $value);
            }
        }
        return $rows;
    }

    /**
     * @param array $parsedCsvRows
     * @return Column[]
     */
    private function createColumns(array $parsedCsvRows): array
    {
        $headers = $parsedCsvRows[0];
        $columnTypes = $this->extractColumnTypes($parsedCsvRows);

        $columns = [];
        foreach ($headers as $index => $header) {
            $columns [] = Column::create($header, $columnTypes[$index] ?? "string");
        }

        return $columns;
    }

    private function extractColumnTypes(array $parsedCsvRows): array
    {
        $types = [];
        foreach (array_slice($parsedCsvRows, 1) as $row) {
            foreach ($row as $columnIndex => $value) {
                $newDataType = $this->getDataType($value);
                $oldDataType = $types[$columnIndex] ?? $newDataType;

                if ($newDataType !== $oldDataType) {
                    $types[$columnIndex] = "string";
                    continue;
                }

                $types[$columnIndex] = $newDataType;
            }
        }
        return $types;
    }

    private function getDataType(string $value): string
    {
        if($value === "true" || $value === "false"){
            return "boolean";
        }

        if(\is_numeric($value)){
            return "number";
        }

        if(\DateTime::createFromFormat(self::ISO_FORMAT, $value) !== false){
            return "dateTime";
        }

        if(\DateTime::createFromFormat(self::DATE_FORMAT, $value) !== false){
            return "date";
        }

        return "string";
    }
}
