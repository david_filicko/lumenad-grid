<?php

namespace App\Http\Controllers;

use App\Contracts\GridParser;
use App\Contracts\GridValidator;
use Exception;
use \Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Symfony\Component\Mime\MimeTypes;

final class GridUploadController extends Controller
{
    public const GRID_FILE_REQUEST_KEY = 'gridFile';

    private GridParser $gridParser;

    private GridValidator $gridValidator;

    public function __construct(GridParser $gridParser, GridValidator $gridValidator)
    {
        $this->gridParser = $gridParser;
        $this->gridValidator = $gridValidator;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $uploadedFile = $request->file(self::GRID_FILE_REQUEST_KEY);
        if(\is_null($uploadedFile)){
            return $this->prepareBadRequest('File not provided');
        }

        if (!$this->isCsv($uploadedFile)){
            return $this->prepareBadRequest("Not a CSV file.");
        }

        $grid = $this->gridParser->parse($uploadedFile->getContent());

        try {
            $this->gridValidator->validate($grid);
        }catch (\Exception $exception){
            return $this->prepareBadRequest($exception->getMessage());
        }

        return (new Response())
            ->setStatusCode(Response::HTTP_OK)
            ->setContent($grid);
    }

    private function prepareBadRequest(string $message): Response
    {
        return (new Response())
            ->setStatusCode(Response::HTTP_BAD_REQUEST)
            ->setContent([
                'error' => $message
            ]);
    }

    private function isCsv(UploadedFile $uploadedFiles): bool
    {
        return \in_array($uploadedFiles->getMimeType(), MimeTypes::getDefault()->getMimeTypes('csv')) ||
            $uploadedFiles->getMimeType() === 'text/plain';
    }
}
