<?php


namespace App\Contracts;


use App\Entity\Grid;

interface GridValidator
{
    public function validate(Grid $grid): void;
}
