<?php


namespace App\Contracts;


use App\Entity\Grid;

interface GridParser
{
    public function parse(string $gridString): Grid;
}
