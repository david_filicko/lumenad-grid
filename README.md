#LumenAd Grid

## Requirements

```
docker >= 20.10 
docker-compose >= 1.29
make >= v4.2
```


## Installation

``make install``

## Usage

In your browser go to:
``http://localhost/upload``
